package ENACTConsumer.logic;

import ENACTConsumer.api.SmoolKP;
import ENACTConsumer.model.smoolcore.impl.ContinuousInformation;
import ENACTConsumer.model.smoolcore.impl.SecurityAuthorization;

/**
 * Example of sending back to SMOOL am actuation message with security data.
 * 
 * <p>
 * Once a temperature value is reached, the temperature Observer calls this
 * class. Then, a new Actuation message on blinds is sent. To prevent other
 * clients to send actuation orders, this message is sent with security content.
 * </P>
 */
class CustomActuation {
	
	private static final String SECURITY_ENDPOINT = "https://15.236.132.74:8443/jwt";
	private boolean isFirstActuation = true;
	private String kpName;
	private String name;
	private double val = 0;
	private static int counter=0;

	SecurityAuthorization sec;
	private ContinuousInformation blindPos;
	
	public CustomActuation(String kpName) {
		this.kpName = kpName;
		this.name = kpName + "_blindActuator";
		blindPos = new ContinuousInformation(name + "_blindPosition");	
		sec = new SecurityAuthorization(name + "_security");
	}
	
	public synchronized void run(double temp) {
		try {
			System.out.println("Sending ACTUATION order for temp = " + temp);
			
			// create the security metadata
			String token = HTTPS.post(SECURITY_ENDPOINT, "{\"sub\":\""+kpName+"\",\"obj\": \"BlindPositionActuator_Kitchen\", \"act\": \"blindDown\"}");
			sec=new SecurityAuthorization(name + "_security"+Integer.toString(counter++));
			sec.setType("JWT + CASBIN payload").setData(token).setTimestamp(Long.toString(System.currentTimeMillis()));
			
			// create the blindPosition information
			blindPos.setValue(val++).setSecurityData(sec).setTimestamp(Long.toString(System.currentTimeMillis()));
			
			// send to SMOOL
			if (isFirstActuation) {
				SmoolKP.getProducer().createBlindPositionActuator(name, kpName, "TECNALIA", null, blindPos, null);
				isFirstActuation=false;
			}else {
				SmoolKP.getProducer().updateBlindPositionActuator(name, kpName, "TECNALIA", null, blindPos, null);
			}
		} catch (Exception e) {
			System.err.println("Error: the actuation order cannot be sent. " + e.getMessage());
		}
	}
	
	
}