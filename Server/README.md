# SMOOL SERVER (SIB)

> Note: for ENACT project, the recommended server is the *Production* server.

When developing KPs, there are three approaches on accessing a SMOOL SIB:
- Production server: the easiest way to connect to SMOOL. KPs are already created with this address/port as the default connection. Currently available at IP 15.236.132.74 and PORT 23000. 
- Eclipse integrated server: If SMOOL updatesite is installed, a local SMOOL server can be started by using the SMOOL perspective in Eclipsee IDE. See https://bitbucket.org/jasonjxm/smool/wiki/Use.
- Standalone server: useful for private IoT networks or for local tests. Just download the latest server from https://bitbucket.org/jasonjxm/smool/downloads/ , configure with your internal IP/port in the file config/SIB.ini, and run the startup.sh or .bat file. More info at https://bitbucket.org/jasonjxm/smool/wiki/Production.

