# CHANGES IN V3 version of ENACT SMOOL CLIENTS

changes:
- **MAJOR** on ENACTProducer, security code for actuation has been removed, because it is now handled from core implementation.
- **minor** all the KPs have a SecurityChecker class to enforce security policies. 

How this new implementation affects on each KP:
- ENACTProducer: this one retrieves the actuation orders sent by the IoT application. The current implementation will applies the "Authorization" policy automatically on that client. This is the main change. For the developer, the change is that the security control code has been removed from the current Observer object (cleaner code in the observer), because it is executed in the core.
- ENACTConsumer: no action, since this client is not subscribed to critical concepts (like actuation orders).
- ENACTSecurity: the `SecurityChecker` class has been tuned to bypass the messages because the business logic for this KP is related to security, so it is better to pass any messages, valid or invalid. See the `SecurityChecker#test()` method in that KP


The new version handles most of the security automatically, by using an `org.smool.security.SecurityChecker` class. This class verify if **any** message received fulfills the security policies. For ENACT, actuation messages should have valid authorization tokens, otherwise the message is rejected. This `SecurityChecker` (or `SecurityEnforcer`) could also be deployed from GeneSIS with more advanced features, like connecting to external security services or performing more specialized security constraints.

The main achievements of the `SecurityChecker` are:
- if SecurityChecker is not found, or no policies have been set -> no action is performed (compatibility with older versions, allow customized security per KP).
- the security code is transparent for KP developers, less code to implement, so focus only in business logic. For advanced users, the security code can be tuned if needed (because it is provided as source code)
- It provides a level of abstraction for third parties, no knowledge of SMOOL is required to implement custom security features on SMOOL KPs, because the `SecurityChecker` extracts all the security related semantic data for every concept. Therefore, external security feaures can be provided as simple policies description, jar library as dependency, or custom SecurityChecker class with connection to external security Services. See `Development and Operation of Trustworthy Smart IoT Systems: The ENACT Framework` research paper.

Example of adding a policy in the `SecurityChecker`, for a JAVA KP:

```java
	public SecurityChecker() {
		policies.put("BlindPositionActuator", "Authorization");
	}
```

How the `SecurityChecker` works:
- SMOOL KP tries to load the class `org.smool.security.SecurityChecker`. If not found, no further action (or deploy your own security implementation if required).
- The default class checks every message ARRIVED (therefore, this is for subscriptions), and returns `true` (if no policy applied or policy is fulfilled) or false (`if policy is not fulfilled`).
- All the SMOOL auto-generated subscription class will check that response. If not valid, the subscription will not emit the subscription event. That is, the message is STOPPED here (so developers are SAFE from malicious messages).
- The developer of KPs will not notice any incoming message, since subscription observers are not triggered. Only valid messages will arrive to the customized `Observer` objects.
- For external security parties, use the `SecurityChecker` as template, and replace it with your own implementation.

See more info at [SMOOL WIKI security page](https://bitbucket.org/jasonjxm/smool/wiki/Security).

See also GeneSIS-SMOOL paper at [Continuous Deployment ofTrustworthy Smart IoT Systems](https://www.researchgate.net/profile/Phu_Nguyen4/publication/341832346_Continuous_Deployment_of_Trustworthy_Smart_IoT_Systems/links/5ed79ab7299bf1c67d350eb5/Continuous-Deployment-of-Trustworthy-Smart-IoT-Systems.pdf).
 

