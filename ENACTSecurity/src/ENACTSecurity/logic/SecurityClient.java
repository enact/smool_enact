package ENACTSecurity.logic;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import ENACTSecurity.logic.HTTPS.MyResponseHandler;

public class SecurityClient {
	private static final String SECURITY_CLIENT_KEY = "SECURITY_CLIENT_KEY";
	private static final String SECURITY_SERVER_URI = "SECURITY_SERVER_URI";

	private final String clientKey;
	private final String server;
	private Map<String,String> headers=new HashMap<>();

	public SecurityClient() throws Exception {
		// configure data for accessing server
		server = System.getenv(SECURITY_SERVER_URI) != null ? System.getenv(SECURITY_SERVER_URI): "https://localhost:8443";
		clientKey = System.getenv(SECURITY_CLIENT_KEY);
		if (server == null || clientKey == null) throw new Exception("CRITICAL: the security server settings are invalid. Recommended action is to stop this client until proper configuration is done");
		// perform a first attempt to check that server is running
		try{
			MyResponseHandler res=HTTPS.get(server);
		}catch(IOException e) {
			throw new Exception("CRITICAL: the security server is not available, or non-compliant. Response code: " + e.getMessage());
		}
	}
	
	public void authenticate() throws Exception {
		String payload = "{\"key\":\"" + clientKey + "\"}";
		MyResponseHandler res = HTTPS.post(server + "/authentication", null, payload);
		if (res.authorization == null) throw new Exception("CRITICAL:  the client cannot authenticate into security server");
		headers.put("Authorization", res.authorization);
	}

	/**
	 * Block a User, KP, or sensor
	 * 
	 * @param id
	 * @throws Exception
	 */
	public void block(String id) throws Exception {
		execute(concat(id, "block"), "");
	}

	public void restart(String id) throws Exception {
		execute(concat(id, "restart"), "");
	}

	/**
	 * Send an alert or a relevant notification.
	 * <p>
	 * The receiver will take the decision based on data. For instance if only first
	 * notification, the security system will be aware of the problem,but next
	 * notifications could prompt the security system to block the client
	 * preemptively.
	 * </p>
	 * 
	 * @param id
	 * @param message
	 * @throws Exception
	 */
	public void notify(String id, String message) throws Exception {
		execute(concat("notify", id), "");
	}

	/**
	 * Send data to security system so this can evaluate and take decision (i.e:
	 * check generic ambient range values for some gas values. These generic rules
	 * could be retrieved from standard public IoT&security repositories)
	 * 
	 * @param id
	 * @param data
	 * @throws Exception
	 */
	// TODO data parameter should be a strict model type, to allow the security
	// engine dispatch the check() to the appropriate algorithm
	public boolean check(String id, String data) throws Exception {
		MyResponseHandler res = execute(concat(id, "check"), data);
		return true;
	}

	private MyResponseHandler execute(String url, String payload) throws Exception {
		return execute(url, payload, true);
	}

	/**
	 * Call the server to execute an action
	 * 
	 * @param params
	 * @throws Exception
	 */
	private MyResponseHandler execute(String url, String payload, boolean isFirstCall) throws Exception {
		MyResponseHandler res;
		try {
			res= HTTPS.post(url,headers,payload);
			return res;
		} catch(Exception e) {
			if (e.getMessage().equals("401") && isFirstCall) {
				// refresh security token and invoke again
				authenticate();
				return execute(url, payload, false);
			} else throw e;
		}
	}

	private String concat(String... paths) {
		StringBuilder sb = new StringBuilder(server);
		sb.append('/');
		sb.append("resources");
		for (String path : paths) {
			sb.append('/');
			sb.append(path);
		}
		return sb.toString();
	}


}
