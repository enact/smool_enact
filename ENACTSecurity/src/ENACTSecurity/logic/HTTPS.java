/**
 * https://memorynotfound.com/ignore-certificate-errors-apache-httpclient/
 * https://www.tutorialspoint.com/apache_httpclient/apache_httpclient_response_handlers.htm
 */

package ENACTSecurity.logic;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.*;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;


public class HTTPS {

//	public static void main(String... args)  throws Exception{
//		//String res=HTTPs.get("https://localhost:8443/jwt");
//		//System.out.println(res);
//		String res = HTTPS.post("https://localhost:8443/jwt","{\"id\":\"aeiou\"}");
//		System.out.println(res);
//	}
	
   public static MyResponseHandler post(String url,Map<String,String> headers,String payload)  throws Exception{
		try (CloseableHttpClient httpclient = createAcceptSelfSignedCertificateClient()) {
		    HttpPost post = new HttpPost(url);
		    HttpEntity stringEntity = new StringEntity(payload,ContentType.TEXT_PLAIN);
		    post.setEntity(stringEntity);
		    if(headers!=null) headers.keySet().forEach(key->post.addHeader(key, headers.get(key)));
		    MyResponseHandler res = new MyResponseHandler();
		    httpclient.execute(post,res);
		    return res;
		} catch (Exception e) {
		    throw e;
		}
    }
		 
   public static MyResponseHandler get(String url)  throws Exception{
        try (CloseableHttpClient httpclient = createAcceptSelfSignedCertificateClient()) {
            HttpGet httpget = new HttpGet(url);
            MyResponseHandler res = new MyResponseHandler();
            httpclient.execute(httpget,res);
            return res;
        } catch (Exception e) {
            throw e;
        }
    }
	    
    public static class MyResponseHandler implements ResponseHandler<String>{
    	public String result = null;
    	public int status = 0;
    	public Header[] headers = null;
    	public String authorization = null;
    	
		public String handleResponse(final HttpResponse res) throws IOException{
			status = res.getStatusLine().getStatusCode();
			if (status >= 400) throw new IOException(Integer.toString(status));
			headers = res.getAllHeaders(); 
			if(res.getFirstHeader("Authorization")!=null) authorization=res.getFirstHeader("Authorization").getValue();
			result = EntityUtils.toString(res.getEntity());
			return "OK";
		}
    }
	    
    private static CloseableHttpClient createAcceptSelfSignedCertificateClient() throws Exception {
        SSLContext sslContext = SSLContextBuilder.create()
            .loadTrustMaterial(new TrustSelfSignedStrategy()).build();
        HostnameVerifier allowAllHosts = new NoopHostnameVerifier();//disable hostname verification
        SSLConnectionSocketFactory connectionFactory = new SSLConnectionSocketFactory(sslContext, allowAllHosts);
        return HttpClients.custom().setSSLSocketFactory(connectionFactory).build();
    }
}

