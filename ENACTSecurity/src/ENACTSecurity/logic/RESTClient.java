package ENACTSecurity.logic;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

/**
 * Basic REST client with no external dependencies and very easy to use.
 */
public class RESTClient {

	public static Response REQUEST(String url, String method, Map<String, String> headers, String payload)
			throws IOException {
		HashMap<String, String> params = new HashMap<>();
		params.put("", payload);
		return REQUEST(url, method, headers, params);
	}

	public static Response REQUEST(String url, String method, Map<String, String> headers, Map<String, String> params)
			throws IOException {
		URLConnection conn = null;
		PrintStream out = null;
		InputStreamReader isr = null;
		BufferedReader br = null;
		try {
			method = method.toUpperCase();
			URL addr = new URL(url);
			conn = addr.openConnection();
			if (headers != null) {
				for (String key : headers.keySet())
					conn.setRequestProperty(key, headers.get(key));
			}
			if (method.contentEquals("GET")) {
				;
			} else {

				if (url.startsWith("https"))
					((HttpsURLConnection) conn).setRequestMethod(method);
				else
					((HttpURLConnection) conn).setRequestMethod(method);
				conn.setDoOutput(true);
				conn.setDoInput(true);
				out = new PrintStream(conn.getOutputStream());
				// if no key, params in json format
				if (params != null) {
					if (params.size() == 1 && params.containsKey("")) {
						String param = params.get("");
						out.print(param);
					}
					// normal params key=value
					else if (params.size() > 0) {
						StringBuilder sbout = new StringBuilder();
						for (String key : params.keySet()) {
							String value = URLEncoder.encode(params.get(key), "UTF-8");
							if (sbout.length() > 0)
								sbout.append("&");
							sbout.append(key);
							sbout.append("=");
							sbout.append(value);
						}
						out.print(sbout.toString());
					}
				}
				out.flush();
			}

			isr = new InputStreamReader(conn.getInputStream());
			br = new BufferedReader(isr);
			StringBuilder sb = new StringBuilder();
			String line;
			while ((line = br.readLine()) != null) {
				sb.append(line);
				sb.append("\n");
			}

			// prepare the response
			int status;

			if (url.startsWith("https"))
				status = ((HttpsURLConnection) conn).getResponseCode();
			else
				status = ((HttpURLConnection) conn).getResponseCode();

			// conn.setDoOutput(false);
			String content = sb.toString();
			if (status >= 400) {
				String msg;

				if (url.startsWith("https"))
					msg = ((HttpsURLConnection) conn).getResponseMessage();
				else
					msg = ((HttpURLConnection) conn).getResponseMessage();

				String response = Integer.toString(status) + " " + msg;
				throw new IOException(response + ": " + content);
			} else {
				return new Response(status, content, conn);
			}
		} catch (IOException ioe) {
			throw ioe;
		} finally {
			if (br != null)
				br.close();
			if (isr != null)
				isr.close();
			if (out != null)
				out.close();
			if (conn != null) {
				if (url.startsWith("https"))
					((HttpsURLConnection) conn).disconnect();
				else
					((HttpURLConnection) conn).disconnect();
			}
		}

	}

	public static class Response {

		public final int status;
		public final String body;
		public final URLConnection conn;

		public Response(int status, String body) {
			this(status, body, null);
		}

		public Response(int status, String body, URLConnection conn) {
			this.status = status;
			this.body = body == null ? "" : body;
			this.conn = conn;
		}

		public boolean isOK() {
			if (status < 400)
				return true;
			else
				return false;
		}

		public String toString() {
			return "{\"status\": " + Integer.toString(status) + " , \"content\": " + body + "}";
		}

	}

}