package ENACTSecurity.logic;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Observer;

import org.smool.kpi.model.exception.KPIModelException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import ENACTSecurity.api.BlindPositionActuatorSubscription;
import ENACTSecurity.api.Consumer;
import ENACTSecurity.api.SmoolKP;
import ENACTSecurity.api.TemperatureSensorSubscription;
import ENACTSecurity.model.smoolcore.IContinuousInformation;
import ENACTSecurity.model.smoolcore.IPhysicalLocation;
import ENACTSecurity.model.smoolcore.impl.BlindPositionActuator;
import ENACTSecurity.model.smoolcore.impl.TemperatureSensor;

/**
 * Subscribe to data generated from KUBIK smart building and check the security
 * (allowed publisher, tampering data, etc)
 */
public class ConsumerMain {
	public static final Gson gson = new GsonBuilder().serializeNulls().create();
	private static SecurityClient security;

	public ConsumerMain(String sib, String addr, int port) throws Exception {
		String name = "EnactSecurity" + System.currentTimeMillis() % 10000;
		SmoolKP.setKPName(name);
		System.out.println("*** " + name + " ***");
		// ---------------------------CONNECT TO SECURITY SERVER---------------------
		try {
			security = new SecurityClient();
		} catch (Exception e) {
			System.err.println(
					"WARNING: the security KP cannot connect to SECURITY server. Advanced security features are disabled.");
			e.printStackTrace();
		}

		// ---------------------------CONNECT TO SMOOL---------------------
		// SmoolKP.connect();
		SmoolKP.connect(sib, addr, port);

		// ---------------------------SUBSCRIBE TO DATA----------------------
		Consumer consumer = SmoolKP.getConsumer();

		TemperatureSensorSubscription tempSubscription = new TemperatureSensorSubscription(createSecurityObserver());
		consumer.subscribeToTemperatureSensor(tempSubscription, null);

		ENACTSecurity.api.BlindPositionActuatorSubscription subscription = new BlindPositionActuatorSubscription(
				createActuationObserver());
		consumer.subscribeToBlindPositionActuator(subscription, null);

		// Thread.sleep(Long.MAX_VALUE); // keep application alive.
		SmoolKP.watchdog(3600); // (seconds) the maximum interval that at least a message should arrive
	}

	private Observer createSecurityObserver() {
		return (o, concept) -> {
			TemperatureSensor sensor = (TemperatureSensor) concept;

			// location, in KUBIK
			IPhysicalLocation loc = sensor.getPhysicalLoc();
			// if (loc==null) System.out.println("LOCATION NULL IS NOT ALLOWED");

			// Tecnalia as the only provider of data in KUBIK
			String vendor = sensor.getVendor();
			if (!vendor.equals("Tecnalia"))
				System.out.println("VENDOR " + vendor + " IS NOT ALLOWED");

			// Only ENACT clients can transmit data into this network
			String individual = sensor._getIndividualID();
			if (!individual.toUpperCase().contains("ENACT"))
				System.out.println("INDIVIDUAL " + individual + " IS NOT REGISTERED");

			// Timestamp must be recent, old data can be DoS attack symptom
			Long timestamp = Long.valueOf(sensor.getTemperature().getTimestamp());
			if (timestamp + 1000 < System.currentTimeMillis())
				System.out.println("TIMESTAMP IS DELAYED");

			// System.out.println("temp from " + individual + ": " +
			// sensor.getTemperature().getValue());
			// System.out.println("message has been checked by ENACT Security system");
			// Advanced demo: check if user in whitelist, otherwise call Security system to
			// block the client and invoke SMOOL to disconnect the device.
			// The security system can call other services to asses if the device (even if
			// it is allowed in the whitelist) is acting different than historical data and
			// also force the disconnection.
			try {
				// String data = gson.toJson(sensor._listSlots());
				String data = sensor.toString();
				if (security != null && security.check(individual, data) == false)
					security.block(individual);
			} catch (Exception e) {
				e.printStackTrace();
			}
		};
	}

	private Observer createActuationObserver() {
		return (o, concept) -> {
			Map<String, Object> record = new HashMap<>();
			BlindPositionActuator actuator = (BlindPositionActuator) concept;
			IContinuousInformation info = actuator.getBlindPos();

			if (info.getSecurityData() == null || info.getSecurityData().getData() == null
					|| info.getSecurityData().getData().equals("")) {
				// insecure actuation orders are discarded
				String message = "CRITICAL: only secure actuation orders are allowed";
				System.out.println(message);

				// OPTIONAL - ASK SECMON TO BLOCK THE DEVICE
				// security.block(individual);

				// SEND ALERT TO SECMON
				record.put("id", info._getIndividualID());
				record.put("timestamp", System.currentTimeMillis());
				record.put("alert", message);
				System.out.println(gson.toJson(record));
			} else {
				// check security is ok
				// TODO call secutron or AC agent to verify order
				System.out.println("receiving ACTUATION order on blinds. Value: " + info.getValue());
				record.put("id", info._getIndividualID());
				Map<String, String> sec = new HashMap<>();
				sec.put("type", info.getSecurityData().getType());
				sec.put("data", info.getSecurityData().getData());
				record.put("security", sec);
				record.put("timestamp", System.currentTimeMillis());
				sec.put("category", info.getSecurityData().getClass().getSimpleName());
				System.out.println(gson.toJson(record));
			}
		};
	}

	private static int counterHealth = 0;

	/**
	 * If multiple disconnection problems, ask SECUTRON to force SIB restarting
	 * 
	 * @throws Exception
	 */
	private static void checkServerHealth(Exception e) throws Exception {
		counterHealth++; // worst case scenario, smool errors and unknown messages (connection,
							// rdfmquery, etc..) -> increment counter
		if (counterHealth > 10) {
			counterHealth = 0;
			System.err.println(
					"CRITICAL:  SMOOL server seems to have problems on reconnecting clients. Asking SECUTRON to restart SMOOL ...");
			security.restart("smool");
		}
	}

	public static void main(String[] args) throws Exception {
		String sib = args.length > 0 ? args[0] : "sib1";
		String addr = args.length > 1 ? args[1] : "15.236.132.74";
		int port = args.length > 2 ? Integer.valueOf(args[2]) : 23000;
		// Logger.setDebugging(true);
		// Logger.setDebugLevel(4);
		while (true) {
			try {
				new ConsumerMain(sib, addr, port);
			} catch (KPIModelException | IOException e) {
				e.printStackTrace();
				//checkServerHealth(e); //ARF: temporary disabled because Kubik sometimes does not send temp data for long time
				Thread.sleep(10000);
				System.out.println("RECONNECTING");
			} catch (Exception e) {
				e.printStackTrace();
				System.exit(1);
			}
		}
	}

}