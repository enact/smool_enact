/**
 * https://www.baeldung.com/httpclient-post-http-request
 * maven or gradle:
 * - compile group: 'org.apache.httpcomponents', name: 'fluent-hc', version: '4.5.7'
 * - compile group: 'org.apache.httpcomponents', name: 'httpclient', version: '4.5.7'
 * - compile group: 'org.apache.httpcomponents', name: 'httpmime', version: '4.5.7'
 *
 */
package ENACTSecurity.logic;

import org.apache.http.HttpResponse;
import org.apache.http.client.fluent.Content;
import org.apache.http.client.fluent.Request;
import org.apache.http.entity.ContentType;

public class TestApacheClient {

	public static void main(String[] args) throws Exception {
		Content content;
		content = Request.Get("https://localhost:8443/actions/check/aaee").execute().returnContent();
		System.out.println(content.toString());
		HttpResponse res = Request.Post("https://localhost:8443/actions/check/EnactPepelu")
				.bodyString("what is up?", ContentType.TEXT_PLAIN).execute().returnResponse();
		System.out.println(res.getStatusLine());
	}
}