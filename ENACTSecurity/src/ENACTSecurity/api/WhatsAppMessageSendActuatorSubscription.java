
/*
 * Generated WhatsAppMessageSendActuatorSubscription
 */

package ENACTSecurity.api;

import ENACTSecurity.model.smoolcore.impl.WhatsAppMessageSendActuator;

import org.smool.kpi.common.Logger;
import org.smool.kpi.model.smart.subscription.AbstractSubscription;
import org.smool.kpi.ssap.message.parameter.SSAPMessageRDFParameter.TypeAttribute;
import java.util.Observer;

public class WhatsAppMessageSendActuatorSubscription extends AbstractSubscription<WhatsAppMessageSendActuator> {

	private Observer customObserver=null;
	
	public WhatsAppMessageSendActuatorSubscription() {
		super(TypeAttribute.RDFM3);
	}
	
	
	public WhatsAppMessageSendActuatorSubscription(Observer customObserver) {
		super(TypeAttribute.RDFM3);
		this.customObserver=customObserver;
	}

	public void conceptAdded(WhatsAppMessageSendActuator aoc) {
		// TODO Add code to handle new added concepts
		Logger.debug("New Concept: " + aoc);
		customNotify(aoc);
	}

	public void conceptRemoved(WhatsAppMessageSendActuator aoc) {
		// TODO Add code to handle removed concepts
		Logger.debug("Removed Concept: " + aoc);
		customNotify(aoc);
	}

	public void conceptUpdated(WhatsAppMessageSendActuator newConcept, WhatsAppMessageSendActuator obsoleteConcept) {
		// TODO Add code to handle updated concepts
		Logger.debug("Updated Concept:");
		Logger.debug("Previous: " + obsoleteConcept);
		Logger.debug("Current: " + newConcept);
		customNotify(newConcept);
	}
	
	private void customNotify(WhatsAppMessageSendActuator concept) {
	  SmoolKP.lastTimestamp = System.currentTimeMillis(); // update last time a message arrived
	  if(customObserver!=null) customObserver.update(null, concept);
	}

}

