
/*
 * Generated HumanDataDeviceSubscription
 */

package ENACTSecurity.api;

import ENACTSecurity.model.smoolcore.impl.HumanDataDevice;

import org.smool.kpi.common.Logger;
import org.smool.kpi.model.smart.subscription.AbstractSubscription;
import org.smool.kpi.ssap.message.parameter.SSAPMessageRDFParameter.TypeAttribute;
import java.util.Observer;

public class HumanDataDeviceSubscription extends AbstractSubscription<HumanDataDevice> {

	private Observer customObserver=null;
	
	public HumanDataDeviceSubscription() {
		super(TypeAttribute.RDFM3);
	}
	
	
	public HumanDataDeviceSubscription(Observer customObserver) {
		super(TypeAttribute.RDFM3);
		this.customObserver=customObserver;
	}

	public void conceptAdded(HumanDataDevice aoc) {
		// TODO Add code to handle new added concepts
		Logger.debug("New Concept: " + aoc);
		customNotify(aoc);
	}

	public void conceptRemoved(HumanDataDevice aoc) {
		// TODO Add code to handle removed concepts
		Logger.debug("Removed Concept: " + aoc);
		customNotify(aoc);
	}

	public void conceptUpdated(HumanDataDevice newConcept, HumanDataDevice obsoleteConcept) {
		// TODO Add code to handle updated concepts
		Logger.debug("Updated Concept:");
		Logger.debug("Previous: " + obsoleteConcept);
		Logger.debug("Current: " + newConcept);
		customNotify(newConcept);
	}
	
	private void customNotify(HumanDataDevice concept) {
	  SmoolKP.lastTimestamp = System.currentTimeMillis(); // update last time a message arrived
	  if(customObserver!=null) customObserver.update(null, concept);
	}

}

