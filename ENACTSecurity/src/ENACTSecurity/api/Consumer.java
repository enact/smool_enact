
/*
 * Generated Consumer interface
 */
package ENACTSecurity.api;


import org.smool.kpi.model.exception.KPIModelException;

import java.util.List;

import ENACTSecurity.model.smoolcore.impl.*;

public interface Consumer {

	/**
	 * Subscribe to the Accelerometer concept
	 *
	 * @param subscription. The subscription object associated to the concept.
	 * @param individualID. An optional individual ID (might be null).
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public void subscribeToAccelerometer(AccelerometerSubscription subscription, String individualID) throws KPIModelException;
	/**
	 * Unsubscribe to the Accelerometer concept
	 *
	 * @param subscription. The subscription object associated to the concept.
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public void unsubscribeToAccelerometer(AccelerometerSubscription subscription) throws KPIModelException;
	/**
	 * Queries for all the Accelerometer concepts
	 *
	 * @return a list of all the individuals of the LightingSensor concept.
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public List<Accelerometer> queryAllAccelerometer() throws KPIModelException;
	/**
	 * Queries for a single Accelerometer concept
	 *
	 * @param individualID. The ID of the queried individual.
	 * @return the concept individual or null.
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public Accelerometer queryAccelerometer(String individualID) throws KPIModelException;
	/**
	 * Subscribe to the BlackoutSensor concept
	 *
	 * @param subscription. The subscription object associated to the concept.
	 * @param individualID. An optional individual ID (might be null).
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public void subscribeToBlackoutSensor(BlackoutSensorSubscription subscription, String individualID) throws KPIModelException;
	/**
	 * Unsubscribe to the BlackoutSensor concept
	 *
	 * @param subscription. The subscription object associated to the concept.
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public void unsubscribeToBlackoutSensor(BlackoutSensorSubscription subscription) throws KPIModelException;
	/**
	 * Queries for all the BlackoutSensor concepts
	 *
	 * @return a list of all the individuals of the LightingSensor concept.
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public List<BlackoutSensor> queryAllBlackoutSensor() throws KPIModelException;
	/**
	 * Queries for a single BlackoutSensor concept
	 *
	 * @param individualID. The ID of the queried individual.
	 * @return the concept individual or null.
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public BlackoutSensor queryBlackoutSensor(String individualID) throws KPIModelException;
	/**
	 * Subscribe to the BlindPositionActuator concept
	 *
	 * @param subscription. The subscription object associated to the concept.
	 * @param individualID. An optional individual ID (might be null).
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public void subscribeToBlindPositionActuator(BlindPositionActuatorSubscription subscription, String individualID) throws KPIModelException;
	/**
	 * Unsubscribe to the BlindPositionActuator concept
	 *
	 * @param subscription. The subscription object associated to the concept.
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public void unsubscribeToBlindPositionActuator(BlindPositionActuatorSubscription subscription) throws KPIModelException;
	/**
	 * Queries for all the BlindPositionActuator concepts
	 *
	 * @return a list of all the individuals of the LightingSensor concept.
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public List<BlindPositionActuator> queryAllBlindPositionActuator() throws KPIModelException;
	/**
	 * Queries for a single BlindPositionActuator concept
	 *
	 * @param individualID. The ID of the queried individual.
	 * @return the concept individual or null.
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public BlindPositionActuator queryBlindPositionActuator(String individualID) throws KPIModelException;
	/**
	 * Subscribe to the EmailSendActuator concept
	 *
	 * @param subscription. The subscription object associated to the concept.
	 * @param individualID. An optional individual ID (might be null).
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public void subscribeToEmailSendActuator(EmailSendActuatorSubscription subscription, String individualID) throws KPIModelException;
	/**
	 * Unsubscribe to the EmailSendActuator concept
	 *
	 * @param subscription. The subscription object associated to the concept.
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public void unsubscribeToEmailSendActuator(EmailSendActuatorSubscription subscription) throws KPIModelException;
	/**
	 * Queries for all the EmailSendActuator concepts
	 *
	 * @return a list of all the individuals of the LightingSensor concept.
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public List<EmailSendActuator> queryAllEmailSendActuator() throws KPIModelException;
	/**
	 * Queries for a single EmailSendActuator concept
	 *
	 * @param individualID. The ID of the queried individual.
	 * @return the concept individual or null.
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public EmailSendActuator queryEmailSendActuator(String individualID) throws KPIModelException;
	/**
	 * Subscribe to the FacebookMessageSendActuator concept
	 *
	 * @param subscription. The subscription object associated to the concept.
	 * @param individualID. An optional individual ID (might be null).
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public void subscribeToFacebookMessageSendActuator(FacebookMessageSendActuatorSubscription subscription, String individualID) throws KPIModelException;
	/**
	 * Unsubscribe to the FacebookMessageSendActuator concept
	 *
	 * @param subscription. The subscription object associated to the concept.
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public void unsubscribeToFacebookMessageSendActuator(FacebookMessageSendActuatorSubscription subscription) throws KPIModelException;
	/**
	 * Queries for all the FacebookMessageSendActuator concepts
	 *
	 * @return a list of all the individuals of the LightingSensor concept.
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public List<FacebookMessageSendActuator> queryAllFacebookMessageSendActuator() throws KPIModelException;
	/**
	 * Queries for a single FacebookMessageSendActuator concept
	 *
	 * @param individualID. The ID of the queried individual.
	 * @return the concept individual or null.
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public FacebookMessageSendActuator queryFacebookMessageSendActuator(String individualID) throws KPIModelException;
	/**
	 * Subscribe to the FloodSensor concept
	 *
	 * @param subscription. The subscription object associated to the concept.
	 * @param individualID. An optional individual ID (might be null).
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public void subscribeToFloodSensor(FloodSensorSubscription subscription, String individualID) throws KPIModelException;
	/**
	 * Unsubscribe to the FloodSensor concept
	 *
	 * @param subscription. The subscription object associated to the concept.
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public void unsubscribeToFloodSensor(FloodSensorSubscription subscription) throws KPIModelException;
	/**
	 * Queries for all the FloodSensor concepts
	 *
	 * @return a list of all the individuals of the LightingSensor concept.
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public List<FloodSensor> queryAllFloodSensor() throws KPIModelException;
	/**
	 * Queries for a single FloodSensor concept
	 *
	 * @param individualID. The ID of the queried individual.
	 * @return the concept individual or null.
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public FloodSensor queryFloodSensor(String individualID) throws KPIModelException;
	/**
	 * Subscribe to the GasSensor concept
	 *
	 * @param subscription. The subscription object associated to the concept.
	 * @param individualID. An optional individual ID (might be null).
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public void subscribeToGasSensor(GasSensorSubscription subscription, String individualID) throws KPIModelException;
	/**
	 * Unsubscribe to the GasSensor concept
	 *
	 * @param subscription. The subscription object associated to the concept.
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public void unsubscribeToGasSensor(GasSensorSubscription subscription) throws KPIModelException;
	/**
	 * Queries for all the GasSensor concepts
	 *
	 * @return a list of all the individuals of the LightingSensor concept.
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public List<GasSensor> queryAllGasSensor() throws KPIModelException;
	/**
	 * Queries for a single GasSensor concept
	 *
	 * @param individualID. The ID of the queried individual.
	 * @return the concept individual or null.
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public GasSensor queryGasSensor(String individualID) throws KPIModelException;
	/**
	 * Subscribe to the HVACActuator concept
	 *
	 * @param subscription. The subscription object associated to the concept.
	 * @param individualID. An optional individual ID (might be null).
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public void subscribeToHVACActuator(HVACActuatorSubscription subscription, String individualID) throws KPIModelException;
	/**
	 * Unsubscribe to the HVACActuator concept
	 *
	 * @param subscription. The subscription object associated to the concept.
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public void unsubscribeToHVACActuator(HVACActuatorSubscription subscription) throws KPIModelException;
	/**
	 * Queries for all the HVACActuator concepts
	 *
	 * @return a list of all the individuals of the LightingSensor concept.
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public List<HVACActuator> queryAllHVACActuator() throws KPIModelException;
	/**
	 * Queries for a single HVACActuator concept
	 *
	 * @param individualID. The ID of the queried individual.
	 * @return the concept individual or null.
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public HVACActuator queryHVACActuator(String individualID) throws KPIModelException;
	/**
	 * Subscribe to the HumanDataDevice concept
	 *
	 * @param subscription. The subscription object associated to the concept.
	 * @param individualID. An optional individual ID (might be null).
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public void subscribeToHumanDataDevice(HumanDataDeviceSubscription subscription, String individualID) throws KPIModelException;
	/**
	 * Unsubscribe to the HumanDataDevice concept
	 *
	 * @param subscription. The subscription object associated to the concept.
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public void unsubscribeToHumanDataDevice(HumanDataDeviceSubscription subscription) throws KPIModelException;
	/**
	 * Queries for all the HumanDataDevice concepts
	 *
	 * @return a list of all the individuals of the LightingSensor concept.
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public List<HumanDataDevice> queryAllHumanDataDevice() throws KPIModelException;
	/**
	 * Queries for a single HumanDataDevice concept
	 *
	 * @param individualID. The ID of the queried individual.
	 * @return the concept individual or null.
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public HumanDataDevice queryHumanDataDevice(String individualID) throws KPIModelException;
	/**
	 * Subscribe to the HumiditySensor concept
	 *
	 * @param subscription. The subscription object associated to the concept.
	 * @param individualID. An optional individual ID (might be null).
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public void subscribeToHumiditySensor(HumiditySensorSubscription subscription, String individualID) throws KPIModelException;
	/**
	 * Unsubscribe to the HumiditySensor concept
	 *
	 * @param subscription. The subscription object associated to the concept.
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public void unsubscribeToHumiditySensor(HumiditySensorSubscription subscription) throws KPIModelException;
	/**
	 * Queries for all the HumiditySensor concepts
	 *
	 * @return a list of all the individuals of the LightingSensor concept.
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public List<HumiditySensor> queryAllHumiditySensor() throws KPIModelException;
	/**
	 * Queries for a single HumiditySensor concept
	 *
	 * @param individualID. The ID of the queried individual.
	 * @return the concept individual or null.
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public HumiditySensor queryHumiditySensor(String individualID) throws KPIModelException;
	/**
	 * Subscribe to the LightRangeActuator concept
	 *
	 * @param subscription. The subscription object associated to the concept.
	 * @param individualID. An optional individual ID (might be null).
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public void subscribeToLightRangeActuator(LightRangeActuatorSubscription subscription, String individualID) throws KPIModelException;
	/**
	 * Unsubscribe to the LightRangeActuator concept
	 *
	 * @param subscription. The subscription object associated to the concept.
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public void unsubscribeToLightRangeActuator(LightRangeActuatorSubscription subscription) throws KPIModelException;
	/**
	 * Queries for all the LightRangeActuator concepts
	 *
	 * @return a list of all the individuals of the LightingSensor concept.
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public List<LightRangeActuator> queryAllLightRangeActuator() throws KPIModelException;
	/**
	 * Queries for a single LightRangeActuator concept
	 *
	 * @param individualID. The ID of the queried individual.
	 * @return the concept individual or null.
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public LightRangeActuator queryLightRangeActuator(String individualID) throws KPIModelException;
	/**
	 * Subscribe to the LightSwitchActuator concept
	 *
	 * @param subscription. The subscription object associated to the concept.
	 * @param individualID. An optional individual ID (might be null).
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public void subscribeToLightSwitchActuator(LightSwitchActuatorSubscription subscription, String individualID) throws KPIModelException;
	/**
	 * Unsubscribe to the LightSwitchActuator concept
	 *
	 * @param subscription. The subscription object associated to the concept.
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public void unsubscribeToLightSwitchActuator(LightSwitchActuatorSubscription subscription) throws KPIModelException;
	/**
	 * Queries for all the LightSwitchActuator concepts
	 *
	 * @return a list of all the individuals of the LightingSensor concept.
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public List<LightSwitchActuator> queryAllLightSwitchActuator() throws KPIModelException;
	/**
	 * Queries for a single LightSwitchActuator concept
	 *
	 * @param individualID. The ID of the queried individual.
	 * @return the concept individual or null.
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public LightSwitchActuator queryLightSwitchActuator(String individualID) throws KPIModelException;
	/**
	 * Subscribe to the LightingSensor concept
	 *
	 * @param subscription. The subscription object associated to the concept.
	 * @param individualID. An optional individual ID (might be null).
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public void subscribeToLightingSensor(LightingSensorSubscription subscription, String individualID) throws KPIModelException;
	/**
	 * Unsubscribe to the LightingSensor concept
	 *
	 * @param subscription. The subscription object associated to the concept.
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public void unsubscribeToLightingSensor(LightingSensorSubscription subscription) throws KPIModelException;
	/**
	 * Queries for all the LightingSensor concepts
	 *
	 * @return a list of all the individuals of the LightingSensor concept.
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public List<LightingSensor> queryAllLightingSensor() throws KPIModelException;
	/**
	 * Queries for a single LightingSensor concept
	 *
	 * @param individualID. The ID of the queried individual.
	 * @return the concept individual or null.
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public LightingSensor queryLightingSensor(String individualID) throws KPIModelException;
	/**
	 * Subscribe to the MessageReceiveSensor concept
	 *
	 * @param subscription. The subscription object associated to the concept.
	 * @param individualID. An optional individual ID (might be null).
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public void subscribeToMessageReceiveSensor(MessageReceiveSensorSubscription subscription, String individualID) throws KPIModelException;
	/**
	 * Unsubscribe to the MessageReceiveSensor concept
	 *
	 * @param subscription. The subscription object associated to the concept.
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public void unsubscribeToMessageReceiveSensor(MessageReceiveSensorSubscription subscription) throws KPIModelException;
	/**
	 * Queries for all the MessageReceiveSensor concepts
	 *
	 * @return a list of all the individuals of the LightingSensor concept.
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public List<MessageReceiveSensor> queryAllMessageReceiveSensor() throws KPIModelException;
	/**
	 * Queries for a single MessageReceiveSensor concept
	 *
	 * @param individualID. The ID of the queried individual.
	 * @return the concept individual or null.
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public MessageReceiveSensor queryMessageReceiveSensor(String individualID) throws KPIModelException;
	/**
	 * Subscribe to the NoiseSensor concept
	 *
	 * @param subscription. The subscription object associated to the concept.
	 * @param individualID. An optional individual ID (might be null).
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public void subscribeToNoiseSensor(NoiseSensorSubscription subscription, String individualID) throws KPIModelException;
	/**
	 * Unsubscribe to the NoiseSensor concept
	 *
	 * @param subscription. The subscription object associated to the concept.
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public void unsubscribeToNoiseSensor(NoiseSensorSubscription subscription) throws KPIModelException;
	/**
	 * Queries for all the NoiseSensor concepts
	 *
	 * @return a list of all the individuals of the LightingSensor concept.
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public List<NoiseSensor> queryAllNoiseSensor() throws KPIModelException;
	/**
	 * Queries for a single NoiseSensor concept
	 *
	 * @param individualID. The ID of the queried individual.
	 * @return the concept individual or null.
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public NoiseSensor queryNoiseSensor(String individualID) throws KPIModelException;
	/**
	 * Subscribe to the PresenceSensor concept
	 *
	 * @param subscription. The subscription object associated to the concept.
	 * @param individualID. An optional individual ID (might be null).
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public void subscribeToPresenceSensor(PresenceSensorSubscription subscription, String individualID) throws KPIModelException;
	/**
	 * Unsubscribe to the PresenceSensor concept
	 *
	 * @param subscription. The subscription object associated to the concept.
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public void unsubscribeToPresenceSensor(PresenceSensorSubscription subscription) throws KPIModelException;
	/**
	 * Queries for all the PresenceSensor concepts
	 *
	 * @return a list of all the individuals of the LightingSensor concept.
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public List<PresenceSensor> queryAllPresenceSensor() throws KPIModelException;
	/**
	 * Queries for a single PresenceSensor concept
	 *
	 * @param individualID. The ID of the queried individual.
	 * @return the concept individual or null.
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public PresenceSensor queryPresenceSensor(String individualID) throws KPIModelException;
	/**
	 * Subscribe to the SMSSendActuator concept
	 *
	 * @param subscription. The subscription object associated to the concept.
	 * @param individualID. An optional individual ID (might be null).
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public void subscribeToSMSSendActuator(SMSSendActuatorSubscription subscription, String individualID) throws KPIModelException;
	/**
	 * Unsubscribe to the SMSSendActuator concept
	 *
	 * @param subscription. The subscription object associated to the concept.
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public void unsubscribeToSMSSendActuator(SMSSendActuatorSubscription subscription) throws KPIModelException;
	/**
	 * Queries for all the SMSSendActuator concepts
	 *
	 * @return a list of all the individuals of the LightingSensor concept.
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public List<SMSSendActuator> queryAllSMSSendActuator() throws KPIModelException;
	/**
	 * Queries for a single SMSSendActuator concept
	 *
	 * @param individualID. The ID of the queried individual.
	 * @return the concept individual or null.
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public SMSSendActuator querySMSSendActuator(String individualID) throws KPIModelException;
	/**
	 * Subscribe to the SmokeSensor concept
	 *
	 * @param subscription. The subscription object associated to the concept.
	 * @param individualID. An optional individual ID (might be null).
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public void subscribeToSmokeSensor(SmokeSensorSubscription subscription, String individualID) throws KPIModelException;
	/**
	 * Unsubscribe to the SmokeSensor concept
	 *
	 * @param subscription. The subscription object associated to the concept.
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public void unsubscribeToSmokeSensor(SmokeSensorSubscription subscription) throws KPIModelException;
	/**
	 * Queries for all the SmokeSensor concepts
	 *
	 * @return a list of all the individuals of the LightingSensor concept.
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public List<SmokeSensor> queryAllSmokeSensor() throws KPIModelException;
	/**
	 * Queries for a single SmokeSensor concept
	 *
	 * @param individualID. The ID of the queried individual.
	 * @return the concept individual or null.
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public SmokeSensor querySmokeSensor(String individualID) throws KPIModelException;
	/**
	 * Subscribe to the TemperatureSensor concept
	 *
	 * @param subscription. The subscription object associated to the concept.
	 * @param individualID. An optional individual ID (might be null).
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public void subscribeToTemperatureSensor(TemperatureSensorSubscription subscription, String individualID) throws KPIModelException;
	/**
	 * Unsubscribe to the TemperatureSensor concept
	 *
	 * @param subscription. The subscription object associated to the concept.
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public void unsubscribeToTemperatureSensor(TemperatureSensorSubscription subscription) throws KPIModelException;
	/**
	 * Queries for all the TemperatureSensor concepts
	 *
	 * @return a list of all the individuals of the LightingSensor concept.
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public List<TemperatureSensor> queryAllTemperatureSensor() throws KPIModelException;
	/**
	 * Queries for a single TemperatureSensor concept
	 *
	 * @param individualID. The ID of the queried individual.
	 * @return the concept individual or null.
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public TemperatureSensor queryTemperatureSensor(String individualID) throws KPIModelException;
	/**
	 * Subscribe to the TweetSendActuator concept
	 *
	 * @param subscription. The subscription object associated to the concept.
	 * @param individualID. An optional individual ID (might be null).
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public void subscribeToTweetSendActuator(TweetSendActuatorSubscription subscription, String individualID) throws KPIModelException;
	/**
	 * Unsubscribe to the TweetSendActuator concept
	 *
	 * @param subscription. The subscription object associated to the concept.
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public void unsubscribeToTweetSendActuator(TweetSendActuatorSubscription subscription) throws KPIModelException;
	/**
	 * Queries for all the TweetSendActuator concepts
	 *
	 * @return a list of all the individuals of the LightingSensor concept.
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public List<TweetSendActuator> queryAllTweetSendActuator() throws KPIModelException;
	/**
	 * Queries for a single TweetSendActuator concept
	 *
	 * @param individualID. The ID of the queried individual.
	 * @return the concept individual or null.
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public TweetSendActuator queryTweetSendActuator(String individualID) throws KPIModelException;
	/**
	 * Subscribe to the WashingMachineActuator concept
	 *
	 * @param subscription. The subscription object associated to the concept.
	 * @param individualID. An optional individual ID (might be null).
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public void subscribeToWashingMachineActuator(WashingMachineActuatorSubscription subscription, String individualID) throws KPIModelException;
	/**
	 * Unsubscribe to the WashingMachineActuator concept
	 *
	 * @param subscription. The subscription object associated to the concept.
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public void unsubscribeToWashingMachineActuator(WashingMachineActuatorSubscription subscription) throws KPIModelException;
	/**
	 * Queries for all the WashingMachineActuator concepts
	 *
	 * @return a list of all the individuals of the LightingSensor concept.
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public List<WashingMachineActuator> queryAllWashingMachineActuator() throws KPIModelException;
	/**
	 * Queries for a single WashingMachineActuator concept
	 *
	 * @param individualID. The ID of the queried individual.
	 * @return the concept individual or null.
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public WashingMachineActuator queryWashingMachineActuator(String individualID) throws KPIModelException;
	/**
	 * Subscribe to the WhatsAppMessageSendActuator concept
	 *
	 * @param subscription. The subscription object associated to the concept.
	 * @param individualID. An optional individual ID (might be null).
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public void subscribeToWhatsAppMessageSendActuator(WhatsAppMessageSendActuatorSubscription subscription, String individualID) throws KPIModelException;
	/**
	 * Unsubscribe to the WhatsAppMessageSendActuator concept
	 *
	 * @param subscription. The subscription object associated to the concept.
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public void unsubscribeToWhatsAppMessageSendActuator(WhatsAppMessageSendActuatorSubscription subscription) throws KPIModelException;
	/**
	 * Queries for all the WhatsAppMessageSendActuator concepts
	 *
	 * @return a list of all the individuals of the LightingSensor concept.
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public List<WhatsAppMessageSendActuator> queryAllWhatsAppMessageSendActuator() throws KPIModelException;
	/**
	 * Queries for a single WhatsAppMessageSendActuator concept
	 *
	 * @param individualID. The ID of the queried individual.
	 * @return the concept individual or null.
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public WhatsAppMessageSendActuator queryWhatsAppMessageSendActuator(String individualID) throws KPIModelException;

}
