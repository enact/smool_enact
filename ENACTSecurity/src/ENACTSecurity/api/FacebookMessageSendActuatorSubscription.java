
/*
 * Generated FacebookMessageSendActuatorSubscription
 */

package ENACTSecurity.api;

import ENACTSecurity.model.smoolcore.impl.FacebookMessageSendActuator;

import org.smool.kpi.common.Logger;
import org.smool.kpi.model.smart.subscription.AbstractSubscription;
import org.smool.kpi.ssap.message.parameter.SSAPMessageRDFParameter.TypeAttribute;
import java.util.Observer;

public class FacebookMessageSendActuatorSubscription extends AbstractSubscription<FacebookMessageSendActuator> {

	private Observer customObserver=null;
	
	public FacebookMessageSendActuatorSubscription() {
		super(TypeAttribute.RDFM3);
	}
	
	
	public FacebookMessageSendActuatorSubscription(Observer customObserver) {
		super(TypeAttribute.RDFM3);
		this.customObserver=customObserver;
	}

	public void conceptAdded(FacebookMessageSendActuator aoc) {
		// TODO Add code to handle new added concepts
		Logger.debug("New Concept: " + aoc);
		customNotify(aoc);
	}

	public void conceptRemoved(FacebookMessageSendActuator aoc) {
		// TODO Add code to handle removed concepts
		Logger.debug("Removed Concept: " + aoc);
		customNotify(aoc);
	}

	public void conceptUpdated(FacebookMessageSendActuator newConcept, FacebookMessageSendActuator obsoleteConcept) {
		// TODO Add code to handle updated concepts
		Logger.debug("Updated Concept:");
		Logger.debug("Previous: " + obsoleteConcept);
		Logger.debug("Current: " + newConcept);
		customNotify(newConcept);
	}
	
	private void customNotify(FacebookMessageSendActuator concept) {
	  SmoolKP.lastTimestamp = System.currentTimeMillis(); // update last time a message arrived
	  if(customObserver!=null) customObserver.update(null, concept);
	}

}

