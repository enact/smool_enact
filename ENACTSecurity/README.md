# Enact security SMOOL KP

This is a SMOOL compliant client(KP) with security features

# Installation

No required, java app provided as source files or as bundled jar.
                                                                                                                                                                                                                                                                                                                                                                                                                   
# Usage


## Pre-requirements

- An SMOOL server must be available
- Add server certificate to java cacerts (see https://stackoverflow.com/questions/9619030/resolving-javax-net-ssl-sslhandshakeexception-sun-security-validator-validatore)

```
sudo keytool -import -v -trustcacerts -alias TECNALIA_SECUTRON_LOCALHOST -file ~/SOFTWARE/ENACT/SOFTWARE/server.crt -keystore /usr/lib/jvm/java-8-oracle/jre/lib/security/cacerts -keypass changeit -storepass changeit

```

## Configuration

> IMPORTANT: BE CAREFUL NOT TO SHARE OR COMMIT THE KEY!!!

 To access secure features provided by external servers, the following elements must be set as ENVIRONMENT variables:
 - SECURITY_CLIENT_KEY: authentication key for that client to be able to connect to security server.
 - [OPTIONAL] SECURITY_SERVER_URI : url address of the security server (this is not the SMOOL server). Default server is https://localhost:8443.
 
 Access data to SMOOL server can be provided in the startup java ARGS parameters
 - [0] the smool server name to be connected to ("sib1","sib","smool_server",...)
 - [1] the smool TCP address (ip or domain)
 - [2] the port where the smool server is listening

## Start

- Just a JAVA application, start the logic.ConsumerMain class from files or from jar file.. The client will subscribe to SMOOL concepts and if security issues are found, the client will invoke either SMOOL server or the external security server to perform actions (blocking a client, checking if client is really valid, etc...).

- The app may contain .sh or .bat files with the configuration to be run. Use them in production environments. Daemon files can also be provided for production, in that case install the appropriate daemon depending on the host OS.
