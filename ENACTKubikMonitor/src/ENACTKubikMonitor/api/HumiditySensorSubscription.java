
/*
 * Generated HumiditySensorSubscription
 */

package ENACTKubikMonitor.api;

import ENACTKubikMonitor.model.smoolcore.impl.HumiditySensor;

import org.smool.kpi.common.Logger;
import org.smool.kpi.model.smart.subscription.AbstractSubscription;
import org.smool.kpi.ssap.message.parameter.SSAPMessageRDFParameter.TypeAttribute;
import java.util.Observer;

public class HumiditySensorSubscription extends AbstractSubscription<HumiditySensor> {

	private Observer customObserver=null;
	
	public HumiditySensorSubscription() {
		super(TypeAttribute.RDFM3);
	}
	
	
	public HumiditySensorSubscription(Observer customObserver) {
		super(TypeAttribute.RDFM3);
		this.customObserver=customObserver;
	}

	public void conceptAdded(HumiditySensor aoc) {
		Logger.debug("New Concept: " + aoc);
		customNotify(aoc); 
	}

	public void conceptRemoved(HumiditySensor aoc) {
		Logger.debug("Removed Concept: " + aoc);
		customNotify(aoc);
	}

	public void conceptUpdated(HumiditySensor newConcept, HumiditySensor obsoleteConcept) {
		Logger.debug("Updated Concept:");
		Logger.debug("Previous: " + obsoleteConcept);
		Logger.debug("Current: " + newConcept);
		customNotify(newConcept);
	}
	
	private void customNotify(HumiditySensor concept) {
	  SmoolKP.lastTimestamp = System.currentTimeMillis(); // update last time a message arrived
	  if(customObserver!=null) customObserver.update(null, concept);
	}

}

