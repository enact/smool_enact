
/*
 * Generated Consumer interface
 */
package ENACTKubikMonitor.api;


import org.smool.kpi.model.exception.KPIModelException;

import java.util.List;

import ENACTKubikMonitor.model.smoolcore.impl.*;

public interface Consumer {

	/**
	 * Subscribe to the HumiditySensor concept
	 *
	 * @param subscription. The subscription object associated to the concept.
	 * @param individualID. An optional individual ID (might be null).
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public void subscribeToHumiditySensor(HumiditySensorSubscription subscription, String individualID) throws KPIModelException;
	/**
	 * Unsubscribe to the HumiditySensor concept
	 *
	 * @param subscription. The subscription object associated to the concept.
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public void unsubscribeToHumiditySensor(HumiditySensorSubscription subscription) throws KPIModelException;
	/**
	 * Queries for all the HumiditySensor concepts
	 *
	 * @return a list of all the individuals of the LightingSensor concept.
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public List<HumiditySensor> queryAllHumiditySensor() throws KPIModelException;
	/**
	 * Queries for a single HumiditySensor concept
	 *
	 * @param individualID. The ID of the queried individual.
	 * @return the concept individual or null.
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public HumiditySensor queryHumiditySensor(String individualID) throws KPIModelException;
	/**
	 * Subscribe to the LightingSensor concept
	 *
	 * @param subscription. The subscription object associated to the concept.
	 * @param individualID. An optional individual ID (might be null).
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public void subscribeToLightingSensor(LightingSensorSubscription subscription, String individualID) throws KPIModelException;
	/**
	 * Unsubscribe to the LightingSensor concept
	 *
	 * @param subscription. The subscription object associated to the concept.
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public void unsubscribeToLightingSensor(LightingSensorSubscription subscription) throws KPIModelException;
	/**
	 * Queries for all the LightingSensor concepts
	 *
	 * @return a list of all the individuals of the LightingSensor concept.
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public List<LightingSensor> queryAllLightingSensor() throws KPIModelException;
	/**
	 * Queries for a single LightingSensor concept
	 *
	 * @param individualID. The ID of the queried individual.
	 * @return the concept individual or null.
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public LightingSensor queryLightingSensor(String individualID) throws KPIModelException;
	/**
	 * Subscribe to the PresenceSensor concept
	 *
	 * @param subscription. The subscription object associated to the concept.
	 * @param individualID. An optional individual ID (might be null).
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public void subscribeToPresenceSensor(PresenceSensorSubscription subscription, String individualID) throws KPIModelException;
	/**
	 * Unsubscribe to the PresenceSensor concept
	 *
	 * @param subscription. The subscription object associated to the concept.
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public void unsubscribeToPresenceSensor(PresenceSensorSubscription subscription) throws KPIModelException;
	/**
	 * Queries for all the PresenceSensor concepts
	 *
	 * @return a list of all the individuals of the LightingSensor concept.
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public List<PresenceSensor> queryAllPresenceSensor() throws KPIModelException;
	/**
	 * Queries for a single PresenceSensor concept
	 *
	 * @param individualID. The ID of the queried individual.
	 * @return the concept individual or null.
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public PresenceSensor queryPresenceSensor(String individualID) throws KPIModelException;
	/**
	 * Subscribe to the TemperatureSensor concept
	 *
	 * @param subscription. The subscription object associated to the concept.
	 * @param individualID. An optional individual ID (might be null).
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public void subscribeToTemperatureSensor(TemperatureSensorSubscription subscription, String individualID) throws KPIModelException;
	/**
	 * Unsubscribe to the TemperatureSensor concept
	 *
	 * @param subscription. The subscription object associated to the concept.
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public void unsubscribeToTemperatureSensor(TemperatureSensorSubscription subscription) throws KPIModelException;
	/**
	 * Queries for all the TemperatureSensor concepts
	 *
	 * @return a list of all the individuals of the LightingSensor concept.
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public List<TemperatureSensor> queryAllTemperatureSensor() throws KPIModelException;
	/**
	 * Queries for a single TemperatureSensor concept
	 *
	 * @param individualID. The ID of the queried individual.
	 * @return the concept individual or null.
	 * @throws KPIModelException. If an error occurs during publishing.
	 */
	public TemperatureSensor queryTemperatureSensor(String individualID) throws KPIModelException;

}
