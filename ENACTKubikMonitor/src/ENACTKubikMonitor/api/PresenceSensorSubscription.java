
/*
 * Generated PresenceSensorSubscription
 */

package ENACTKubikMonitor.api;

import ENACTKubikMonitor.model.smoolcore.impl.PresenceSensor;

import org.smool.kpi.common.Logger;
import org.smool.kpi.model.smart.subscription.AbstractSubscription;
import org.smool.kpi.ssap.message.parameter.SSAPMessageRDFParameter.TypeAttribute;
import java.util.Observer;

public class PresenceSensorSubscription extends AbstractSubscription<PresenceSensor> {

	private Observer customObserver=null;
	
	public PresenceSensorSubscription() {
		super(TypeAttribute.RDFM3);
	}
	
	
	public PresenceSensorSubscription(Observer customObserver) {
		super(TypeAttribute.RDFM3);
		this.customObserver=customObserver;
	}

	public void conceptAdded(PresenceSensor aoc) {
		Logger.debug("New Concept: " + aoc);
		customNotify(aoc); 
	}

	public void conceptRemoved(PresenceSensor aoc) {
		Logger.debug("Removed Concept: " + aoc);
		customNotify(aoc);
	}

	public void conceptUpdated(PresenceSensor newConcept, PresenceSensor obsoleteConcept) {
		Logger.debug("Updated Concept:");
		Logger.debug("Previous: " + obsoleteConcept);
		Logger.debug("Current: " + newConcept);
		customNotify(newConcept);
	}
	
	private void customNotify(PresenceSensor concept) {
	  SmoolKP.lastTimestamp = System.currentTimeMillis(); // update last time a message arrived
	  if(customObserver!=null) customObserver.update(null, concept);
	}

}

