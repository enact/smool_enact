
/*
 * Generated TemperatureSensorSubscription
 */

package ENACTKubikMonitor.api;

import ENACTKubikMonitor.model.smoolcore.impl.TemperatureSensor;

import org.smool.kpi.common.Logger;
import org.smool.kpi.model.smart.subscription.AbstractSubscription;
import org.smool.kpi.ssap.message.parameter.SSAPMessageRDFParameter.TypeAttribute;
import java.util.Observer;

public class TemperatureSensorSubscription extends AbstractSubscription<TemperatureSensor> {

	private Observer customObserver=null;
	
	public TemperatureSensorSubscription() {
		super(TypeAttribute.RDFM3);
	}
	
	
	public TemperatureSensorSubscription(Observer customObserver) {
		super(TypeAttribute.RDFM3);
		this.customObserver=customObserver;
	}

	public void conceptAdded(TemperatureSensor aoc) {
		Logger.debug("New Concept: " + aoc);
		customNotify(aoc); 
	}

	public void conceptRemoved(TemperatureSensor aoc) {
		Logger.debug("Removed Concept: " + aoc);
		customNotify(aoc);
	}

	public void conceptUpdated(TemperatureSensor newConcept, TemperatureSensor obsoleteConcept) {
		Logger.debug("Updated Concept:");
		Logger.debug("Previous: " + obsoleteConcept);
		Logger.debug("Current: " + newConcept);
		customNotify(newConcept);
	}
	
	private void customNotify(TemperatureSensor concept) {
	  SmoolKP.lastTimestamp = System.currentTimeMillis(); // update last time a message arrived
	  if(customObserver!=null) customObserver.update(null, concept);
	}

}

