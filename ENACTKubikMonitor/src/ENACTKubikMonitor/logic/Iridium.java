package ENACTKubikMonitor.logic;

import java.util.HashMap;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tecnalia.util.rest.RESTClient;
import com.tecnalia.util.rest.RESTClient.Response;

/**
 * Proxy for accessing IRIDIUM registry services
 */
public class Iridium {
	private static final String URL="https://rcc.esilab.org/registry/resources";
	//private static final String URL="http://127.0.0.1/registry/resources";
	public static final Gson gson = new GsonBuilder().serializeNulls().create();
	
	@SuppressWarnings("unchecked")
	public static void PUT(String id, String value, int status) throws Exception {
		String url=URL+"/"+id;
		Response response=RESTClient.request(url, "get", null, "");
		Map<String,Object> map = new HashMap<String,Object>();
		map = (Map<String,Object>) gson.fromJson(response.content, map.getClass());
		map.put("timestamp", System.currentTimeMillis());
		map.put("value", value);
		map.put("status", status);
		String result=gson.toJson(map);
		RESTClient.request(url, "put", null, result);
//		System.out.println(result);
	}
		
}
