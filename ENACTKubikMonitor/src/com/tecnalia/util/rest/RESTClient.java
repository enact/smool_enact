package com.tecnalia.util.rest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

/**
 * Basic REST client with no external dependencies and very easy to use.
 */
public class RESTClient {

	public static Response request(String url, String method, Map<String, String> headers, String payload)
			throws IOException {
		HashMap<String, String> params = new HashMap<>();
		params.put("", payload);
		return request(url, method, headers, params);
	}

	public static Response request(String url, String method, Map<String, String> headers, Map<String, String> params)
			throws IOException {
		HttpURLConnection conn = null;
		PrintStream out = null;
		InputStreamReader isr = null;
		BufferedReader br = null;
		try {
			method = method.toUpperCase();
			URL addr = new URL(url);
			conn = (HttpURLConnection) addr.openConnection();
			if (headers != null) {
				for (String key : headers.keySet())
					conn.setRequestProperty(key, headers.get(key));
			}
			if (method.contentEquals("GET")) {
				;
			} else {
				conn.setRequestMethod(method);
				conn.setDoOutput(true);
				out = new PrintStream(conn.getOutputStream());
				// if no key, params in json format
				if (params != null) {
					if (params.size() == 1 && params.containsKey("")) {
						String param = params.get("");
						out.print(param);
					}
					// normal params key=value
					else if (params.size() > 0) {
						StringBuilder sbout = new StringBuilder();
						for (String key : params.keySet()) {
							String value = URLEncoder.encode(params.get(key), "UTF-8");
							if (sbout.length() > 0)
								sbout.append("&");
							sbout.append(key);
							sbout.append("=");
							sbout.append(value);
						}
						out.print(sbout.toString());
					}
				}
				out.flush();
			}

			isr = new InputStreamReader(conn.getInputStream());
			br = new BufferedReader(isr);
			StringBuilder sb = new StringBuilder();
			String line;
			while ((line = br.readLine()) != null) {
				sb.append(line);
				sb.append("\n");
			}

			// prepare the response
			int status = conn.getResponseCode();
			String content = sb.toString();
			if (status >= 400) {
				String response = Integer.toString(status) + " " + conn.getResponseMessage();
				throw new IOException(response + ": " + content);
			} else {
				return new Response(status, content);
			}
		} catch (IOException ioe) {
			throw ioe;
		} finally {
			if (br != null)
				br.close();
			if (isr != null)
				isr.close();
			if (out != null)
				out.close();
			if (conn != null)
				conn.disconnect();
		}

	}

	public static class Response {

		public final int status;
		public final String content;

		public Response(int status, String content) {
			this.status = status;
			if (content == null)
				content = "";
			this.content = content;
		}

		public boolean isOK() {
			if (status < 400)
				return true;
			else
				return false;
		}

		public String toString() {
			return "{\"status\": " + Integer.toString(status) + " , \"content\": " + content + "}";
		}

	}

}