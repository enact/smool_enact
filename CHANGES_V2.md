# CHANGES IN V2 version of ENACT SMOOL CLIENTS

## For all KP clients

**MAJOR**: the ontology has **security concepts** that can be attached to sensors or actuations information data. All security data has two fields `data` containing the payload to be sent (a token, encrypted data, etc..) and the `type` optional data (if data encrypted, the type of algorithm can be sent here).

![](ontology_security.png)

## For the KP producer

**MAJOR**: the producer can send or receive data with security concepts inside the information objects

```java
info.getSecurityData();
```


**MAJOR**: In V2 the producer is still sending sensor data but it can also receive Actuation orders from the IoT app (the consumer project). Example:

```java
private Observer createObserver() {
    ...
    BlindPositionActuator actuator = (BlindPositionActuator) concept;
    IContinuousInformation info = actuator.getBlindPos();
    System.out.println(info.getValue()+info.getSecurityData().getData());
}
```


## For the KP consumer

**MAJOR**: **the consumer can check for unexpected TCP connection issues**, even if the routers-in-the-middle close the connection (for instance when accesing server from internet, the routers can close the connections even if KEEP_ALIVE is properly set for client and server), cable disconnections (the connection in the client is still valid and not closed)a on other unexpected problems. Just set the new `watchdog(seconds)` method instead of the `Thread.sleep(n)` in the consumers. If messages are expected to be updated every 10 seconds a watchdog(60) is OK. If sensors are sending data at least once a day, use a bigger timeout (watchdog(24*3600)). Note: the expected TCP issues are already managed by SMOOL, but this method can warn if no messages are being received and thay should.

```java
  // -----------ATTACH WATCHDOG instead of SLEEP-------
  // Thread.sleep(Long.MAX_VALUE); // keep application alive.
  SmoolKP.watchdog(5 * 60); // 5 min is the maximum interval that at least one message should arrive
```

**MAJOR**: the consumer can send or receive data with security concepts inside the information. This is useful when sending actuation orders, so only authorized clients can send valid orders. 

```java
info.setSecurityData(securityObject);
```

**MAJOR**: the consumer sends Actuation orders on Blinds when temperature is higher than a threshold. As example, there is a new class CustomActuation.java. This class sticks a security object into the actuation order, so the receiver could check if the order is authorized before executing the action

```java
class CustomActuation {
  private SecurityAuthorization sec;
  private BlindPositionActuator actuator;
  private ContinuousInformation blindPos;

  public void run(double temp) {
    sec.setData("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ...");
    blindPos.setValue(val).setSecurityData(sec);
    actuator.setBlindPos(blindPos);
    SmoolKP.getProducer().createBlindPositionActuator(name, kpName, "TECNALIA", null, blindPos, null);
  }
}
```

## For the security KP

**MAJOR**: the security KP can check (and block) clients sending insecure actuation orders.

**MAJOR**: the security KP can subscribe to security concepts standalone (to verify and notify problems) instead of susbcribing on each sensor/information flowing around SMOOL.

**minor**: the security KP can restart the SMOOL server (SIB) if connection problems persist on healthy KPs( for instance while a DoS attack) 


# Adaptation of the Enact Smart Building use case for the secure Actuation orders

1. The sensors are sending data, the data is received by the IoT app (temperature above 25º will trigger an action from the IoT app)

![](enact_send_temperature.png)

2. The IoT app sends an Actuation order to lower the temperature. The receiver checks that the order is secure, and then asks the SCADA to execute the real order.

![](enact_execute_actuation.png)
